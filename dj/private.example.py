# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*m=3#cc72j29^=j4c7^pf34l5+)-59t12f0yni5e-0qdt-79ed'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = 1
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


ALLOWED_HOSTS = [
    '127.0.0.1','localhost','*',
]


EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'test@gmail.com'
EMAIL_HOST_PASSWORD = 'test'
EMAIL_PORT = 587