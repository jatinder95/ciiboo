"""dj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
1. Add an import:  from my_app import views
2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
1. Add an import:  from other_app.views import Home
2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
1. Import the include() function: from django.conf.urls import url, include
2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.contrib.auth.views import logout
from account.views import register,register2,login,update,confirm,verify,reset,forgot,testEmail,deleteUser,approveUser,declineUser

urlpatterns = [
	
	url(r'^register/restaurant', register2,name='register2'),
	url(r'^testemail/', testEmail,name='register'),
	url(r'^register/', register,name='register'),
	url(r'^delete/user/(?P<id>[0-9]+)', deleteUser,name='deleteUser'),
	url(r'^approve/appr/(?P<id>[0-9]+)', approveUser,name='approveUser'),
	url(r'^approve/decline/(?P<id>[0-9]+)', declineUser,name='declineUser'),
	url(r'^login/$', login, name='login'),
	url(r'^update/$', update, name='update'),
	url(r'^reset/(?P<token>[-\w]+)$', reset, name='reset'),
	url(r'^forgot/$', forgot, name='forgot'),
	url(r'^confirm/$', confirm, name='confirm'),
	url(r'^verify/(?P<token>[-\w]+)$', verify, name='verify'),
	url(r'^logout/$', logout, {'next_page': '/'}, name='logout'),
	url(r'^dashboard/', include('dashboard.urls')),
	url('^api/v1/', include('social_django.urls', namespace='social')),
	url(r'^articles/', include('blog.urls')),
	url(r'^restaurants/', include('restaurant.urls')),
	url(r'^order/', include('cart.urls')),
	url(r'^admin/', admin.site.urls),
	url(r'^', include('pages.urls')),
]