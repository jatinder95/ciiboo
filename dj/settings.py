import os

from .private import *

LOGIN_REDIRECT_URL = 'http://localhost:8040/'
LOGIN_URL = 'login'
LOGOUT_URL = 'logout'

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
STATIC_URL = '/static/'
# STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = (
  os.path.join(BASE_DIR, 'static/'),
)

YELP_KEY='Leil2XFe0HDD3IABRROLAA'
YELP_SECRET='nrrBrftvUPZrnb9OVj2BrGwfZdim9PYegP4JjNuBtfiLH0miADor2b05MU2iUBHN'

MEDIA_URL = '/static/uploads/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'static/uploads')

SITE_NAME = 'CiiBoo'
AUTH_USER_MODEL = 'account.User'

INSTALLED_APPS = [
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'account',
	'social_django',
	'blog',
	'pages',
	'restaurant',
]

MIDDLEWARE = [
	'django.middleware.security.SecurityMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'dj.urls'

TEMPLATES = [
	{
		'BACKEND': 'django.template.backends.django.DjangoTemplates',
		'DIRS': [
			os.path.join(BASE_DIR, "templates"),
		],
		'APP_DIRS': True,
		'OPTIONS': {
			'context_processors': [
				'django.template.context_processors.debug',
				'django.template.context_processors.request',
				'django.contrib.auth.context_processors.auth',
				'django.contrib.messages.context_processors.messages',
			],
		},
	},
]

WSGI_APPLICATION = 'dj.wsgi.application'
AUTH_PASSWORD_VALIDATORS = [
	{
		'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
	},
]


LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
APPEND_SLASH=1

SOCIAL_AUTH_FACEBOOK_KEY = '378555979266988'  # App ID
SOCIAL_AUTH_FACEBOOK_SECRET = '731cdefcf877bd918d73ca23af263c6b'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'fields': 'name,email', 
}
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY ='235236880028-12fmg5obot6o1kgn4sh7rb0rdjo4ili7.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET ='ERErt9MsV3a3l6n6M2EC7OgP'

AUTHENTICATION_BACKENDS = (
    'social_core.backends.google.GoogleOAuth2',
	
	'social_core.backends.facebook.FacebookOAuth2',
	'django.contrib.auth.backends.ModelBackend',
    
)
# SOCIAL_AUTH_GOOGLE_KEY = '99374919041-1qkqdbctk7pi9qpahr43add5aji1bn4n.apps.googleusercontent.com'
# SOCIAL_AUTH_GOOGLE_SECRET = 'g0YZfHDB75iQ1uMOhxeqysj1'

# SOCIAL_AUTH_FACEBOOK_KEY = '132736897330224'
# SOCIAL_AUTH_FACEBOOK_SECRET = '315703911d3c4d1f3c1ecf817d2c6ecd'