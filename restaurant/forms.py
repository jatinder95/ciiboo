# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .models import Restaurant,Cuisine,Section,countries
from django import forms



class RestaurantForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
				super(RestaurantForm, self).__init__(*args, **kwargs)
				for f in self.fields:
					self.fields[f].widget.attrs['placeholder'] = self.fields[f].label
	class Meta:
		model = Restaurant
		widgets = {
			'country': forms.Select(choices=countries)
			,'yelp_link': forms.TextInput(attrs={'required':'required'})
			,'website': forms.TextInput(attrs={'required':'required'})}
			
		fields = ['title','country','street','zip_code','phone','city','state','category','picture' ,'banner','website','content','deliver','yelp_link']

class SectionForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
				super(SectionForm, self).__init__(*args, **kwargs)
				for f in self.fields:
					self.fields[f].widget.attrs['placeholder'] = self.fields[f].label
	class Meta:
		model = Section
		fields = ['title']

class CuisineForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
				super(CuisineForm, self).__init__(*args, **kwargs)
				for f in self.fields:
					self.fields[f].widget.attrs['placeholder'] = self.fields[f].label
	class Meta:
		model = Cuisine
		fields = ['title','content','picture','price']