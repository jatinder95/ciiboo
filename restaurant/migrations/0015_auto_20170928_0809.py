# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-28 08:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restaurant', '0014_remove_restaurant_created'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurant',
            name='created',
            field=models.DateField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='cuisine',
            name='picture',
            field=models.ImageField(blank=True, null=True, upload_to='cuisine_images'),
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='banner',
            field=models.ImageField(blank=True, null=True, upload_to='restaurantBanners'),
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='picture',
            field=models.ImageField(blank=True, null=True, upload_to='restaurantImages'),
        ),
    ]
