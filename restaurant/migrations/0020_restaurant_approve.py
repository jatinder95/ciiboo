# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-11-27 04:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restaurant', '0019_restaurant_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurant',
            name='approve',
            field=models.BooleanField(default=False),
        ),
    ]
