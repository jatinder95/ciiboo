# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect,get_object_or_404
from .models import Restaurant,Cuisine,Section,Order,Item,countries
from .forms import RestaurantForm,CuisineForm,SectionForm
from shorty import onlyOwner,onlyEditor,onlyAdmin,onlyAuth
from dj.settings import YELP_SECRET, YELP_KEY
import json,requests

def cancel(request,id):
	restaurant = get_object_or_404(Restaurant, id=id)
	Order.objects.filter(user=request.user,restaurant=restaurant).delete()
	return redirect(request.META.get('HTTP_REFERER'))

def index(request):
	restaurants = Restaurant.objects.filter(approve=True)
	context = {'restaurants': restaurants}
	return render(request, 'restaurant/index.html', context)

@onlyEditor
def create(request):
	form = RestaurantForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		a=form.save(commit=False)
		a.user=request.user
		a.save()
		return redirect('/restaurants/')
	return render(request, 'restaurant/create.html', {'form': form})

@onlyOwner
def edit(request,id):
	restaurant = get_object_or_404(Restaurant, id=id)
	print(id)
	form = RestaurantForm(request.POST or None, request.FILES or None, instance=restaurant)
	print request.POST
	if form.is_valid():
		form.save()
		# return redirect('/restaurants/'+str(restaurant.id))
		return redirect('/dashboard/info')
	return render(request, 'restaurant/edit.html', {'form': form,'restaurant':restaurant})

@onlyAdmin
def delete(request,id):
	restaurant = get_object_or_404(Restaurant, id=id)
	restaurant.delete()
	return redirect('/restaurants/')

@onlyOwner
def createCuisine(request,id,sid):
	restaurant = get_object_or_404(Restaurant, id=id)
	section = get_object_or_404(Section, id=sid)
	form = CuisineForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		a=form.save(commit=False)
		a.section=section
		a.save()
		# return redirect('/restaurants/'+str(restaurant.id))
		return redirect('/dashboard/menu')
	return render(request, 'restaurant/cuisine_create.html', {'form': form,'restaurant':restaurant})

@onlyOwner
def createSection(request,id):
	restaurant = get_object_or_404(Restaurant, id=id)
	form = SectionForm(request.POST or None)
	if form.is_valid():
		a=form.save(commit=False)
		a.restaurant=restaurant
		a.save()
		# return redirect('/restaurants/'+str(restaurant.id))
		return redirect('/dashboard/menu')
	return render(request, 'restaurant/section_create.html', {'form': form,'restaurant':restaurant})

@onlyOwner
def editCuisine(request,id,pid):
	restaurant = get_object_or_404(Restaurant, id=id)
	cuisine = get_object_or_404(Cuisine, id=pid)
	form = CuisineForm(request.POST or None, request.FILES or None, instance=cuisine)
	if form.is_valid():
		form.save()
		# return redirect('/restaurants/'+str(restaurant.id))
		return redirect('/dashboard/menu')
	return render(request, 'restaurant/cuisine_edit.html', {'form': form,'restaurant':restaurant,'cuisine':cuisine})

@onlyOwner
def editSection(request,id,pid):
	restaurant = get_object_or_404(Restaurant, id=id)
	section = get_object_or_404(Section, id=pid)
	form = CuisineForm(request.POST or None, instance=section)
	if form.is_valid():
		form.save()
		# return redirect('/restaurants/'+str(restaurant.id)+'/section/'+str(pid))
		return redirect('/dashboard/menu')
	return render(request, 'restaurant/cuisine_edit.html', {'form': form,'restaurant':restaurant,'section':section})

def getYelpId(restaurant):
	try:
		return restaurant.yelp_link.split('biz/')[1].split('?')[0]
	except:
		return 0

def getRating(r):
	yelp_id=getYelpId(r)
	rating=0
	if yelp_id:
		req=requests.post('https://api.yelp.com/oauth2/token',{'grant_type':'client_credentials','client_id':YELP_KEY,'client_secret':YELP_SECRET})
		token=json.loads(req.content)['access_token']
		print token
		headers={'Authorization':'Bearer '+token}
		url='https://api.yelp.com/v3/businesses/'+yelp_id
		req=requests.get(url,headers=headers)
		try:
			rating=req.json()['rating']
			print 'rating',rating
		except:
			print 'No Rating'
	return rating

	
def dele(request,id):
	item = get_object_or_404(Item, id=id)
	item.delete()
	return redirect(request.META.get('HTTP_REFERER'))
	
def show(request, id):
	sectionform = SectionForm()
	restaurant = get_object_or_404(Restaurant, id=id)
	restaurant.country=dict(countries)[restaurant.country]
	sections= Section.objects.filter(restaurant=restaurant)
	restaurant.rating=getRating(restaurant)
	for section in sections:
		section.cuisines=Cuisine.objects.filter(section=section)
	order={}
	items={}
	total=0
	if request.user.is_authenticated:
		oldOrders = Order.objects.filter(user=request.user,restaurant=restaurant).order_by('-id')
		if oldOrders:
			oldOrder=oldOrders[0]
			if oldOrder.status<3:
				order=oldOrder
				items= Item.objects.filter(order=order)
				for i in items:
					amount=i.cuisine.price*i.quantity
					i.amount=amount
					total=total+amount
	return render(request, 'restaurant/show.html', {'restaurant': restaurant,'sections':sections,'order':order,'items':items,'total':total,'sectionform':sectionform})

@onlyAuth
def addFood(request, id,pid):
	restaurant = get_object_or_404(Restaurant, id=id)
	cuisine = get_object_or_404(Cuisine, id=pid)
	order={}
	oldOrders = Order.objects.filter(user=request.user,restaurant=restaurant).order_by('-id')
	if oldOrders:
		oldOrder=oldOrders[0]
		if oldOrder.status<3:
			order=oldOrder
	if not order:
		order=Order(restaurant_id=restaurant.id,user_id=request.user.id)
		order.save()
	item=Item.objects.filter(cuisine=cuisine,order=order)
	if item:
		item=item[0]
		item.quantity=item.quantity+1
	else:
		item=Item(cuisine_id=cuisine.id,order_id=order.id)
	item.save()
	return redirect(request.META.get('HTTP_REFERER'))

@onlyOwner
def deleteCuisine(request,id,pid):
	restaurant = get_object_or_404(Restaurant, id=id)
	cuisine = get_object_or_404(Cuisine, id=pid)
	cuisine.delete()
	# return redirect('/restaurants/'+str(restaurant.id))
	return redirect('/dashboard/menu')
