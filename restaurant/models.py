# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from account.models import Address
from dj import settings

ost=['Started',
'Choosing Address',
'Adding Payment',
'Payment Processing',
'Order Placed',
'Confirmed',
'Preparing',
'On the Way',
'Delivered']

countries= (
	(0,'United States'),
	(1,'Canada'),
	(2,'India')
)

class Restaurant(models.Model):
	created = models.DateField(auto_now_add=True,null=True, blank=True)
	title = models.CharField(verbose_name='Business Name' ,max_length=200)
	country = models.IntegerField(verbose_name='Country / Region',choices=countries,default=0)
	street = models.CharField(verbose_name='Street Address',max_length=200)
	city = models.CharField(max_length=200)
	state = models.CharField(max_length=200)
	zip_code = models.CharField(max_length=200)
	approve = models.BooleanField(default=False)
	phone = models.CharField(verbose_name='Business Phone',max_length=200,null=True)
	category = models.CharField(max_length=200)
	website = models.CharField(max_length=200,null=True, blank=True, default = None)
	yelp_link = models.CharField(max_length=200,null=True, blank=True, default = None)
	deliver = models.BooleanField(verbose_name='Deivers to Customer location',default=True)
	rating = models.DecimalField(max_digits=4, decimal_places=2,default=1)
	cost = models.IntegerField(default=1)
	content = models.TextField(verbose_name='Business Description')
	banner=models.ImageField(verbose_name='Banner',upload_to='restaurantBanners',null=True, blank=True)
	picture=models.ImageField(upload_to='restaurantImages',null=True, blank=True)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	stripeId = models.CharField(max_length=200,null=True, blank=True)

class Section(models.Model):
	title = models.CharField(max_length=200)
	restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)

class Cuisine(models.Model):
	title = models.CharField(verbose_name='Cuisine',max_length=200)
	content = models.TextField()
	price = models.DecimalField(max_digits=8, decimal_places=2)
	picture=models.ImageField(upload_to='cuisine_images',null=True, blank=True)
	section = models.ForeignKey(Section, null=True, blank=True, default = None)

class Order(models.Model):
	created = models.DateField(auto_now_add=True,null=True, blank=True)
	restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
	status = models.IntegerField(default=0)
	delivery = models.BooleanField(default=True)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	address = models.ForeignKey(Address, null=True, blank=True, default = None)

class Item(models.Model):
	order = models.ForeignKey(Order, on_delete=models.CASCADE)
	cuisine = models.ForeignKey(Cuisine, on_delete=models.CASCADE)
	quantity = models.IntegerField(default=1)