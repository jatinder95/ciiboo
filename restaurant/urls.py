from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^create', views.create, name='create'),
    url(r'^(?P<id>[0-9]+)/category/(?P<sid>[0-9]+)/cuisine/create', views.createCuisine, name='createCuisine'),
    url(r'^(?P<id>[0-9]+)/category/create', views.createSection, name='createSection'),
    url(r'^(?P<id>[0-9]+)/cuisine/(?P<pid>[0-9]+)/add$', views.addFood, name='addCuisine'),
    url(r'^(?P<id>[0-9]+)/cuisine/(?P<pid>[0-9]+)/edit', views.editCuisine, name='editCuisine'),
    url(r'^(?P<id>[0-9]+)/cuisine/(?P<pid>[0-9]+)/delete', views.deleteCuisine, name='deleteCuisine'),
    url(r'^(?P<id>[0-9]+)/edit', views.edit, name='edit'),
    url(r'^(?P<id>[0-9]+)/delete', views.delete, name='delete'),
	url(r'^(?P<id>[0-9]+)/dele$', views.dele, name='dele'),
	#url(r'^(?P<id>[0-9]+)/delivery$', views.order, name='delivery'),
    url(r'^(?P<id>[0-9]+)/$', views.show, name='show'),
    url(r'^(?P<id>[0-9]+)/cancel$', views.cancel, name='cancel'),
    
]