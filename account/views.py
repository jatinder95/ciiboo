from django.contrib.auth import login as auth
from django.shortcuts import render,redirect,get_object_or_404
from .models import User
from restaurant.models import Restaurant
from restaurant.forms import RestaurantForm
from .forms import ConfirmForm, LoginForm, RegisterForm, OwnerForm, ResetForm, validatePassword,EditProfile
import random, string
from django.db.models import Q
from django.core.mail import EmailMessage
from dj.private import SITE_URL
from shorty import onlyOwner,onlyEditor,onlyAdmin,onlyAuth

def authenticate(username=None, password=None,
	backend='django.contrib.auth.backends.ModelBackend'):
	try:
		user = User.objects.get(Q(email__iexact=username))
		
		if user.check_password(password):
			return user
		# if user.password==password:
			# return user
		return 0
	except User.DoesNotExist:
		return 0

def randomword(length):
	return ''.join(random.choice(string.lowercase) for i in range(length))


def login(request):
	
	error=''
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			data=form.cleaned_data
			print data
			user = authenticate(username=data['username'], password=data['password'])
			if user:
				if user.token:
					sendConfirm(user)
					return redirect('confirm')
				auth(request, user,backend='django.contrib.auth.backends.ModelBackend')
				return redirect('home')
			else:
				error='Invalid Credentials'
	else:
		form = LoginForm()
	return render(request, 'account/login.html', {'form': form,'error':error})

def saveUser(data):
	print 'data'
	if 'confirm_password' in data:
		del data['confirm_password']
	print data
	u=User(**data)
	p=data['password']
	u.save()
	u.set_password(p)
	u.save()
	return u

def register(request):
	if request.method == 'POST':
		form = RegisterForm(request.POST)
		if form.is_valid():
			data=form.cleaned_data
			data['editor']=0
			token=randomword(20)
			data['token']=token		
			# validatePassword(data['password'])
			u=saveUser(data)
			sendConfirm(u)
			return redirect('confirm')
	else:
		form = RegisterForm()
	return render(request, 'account/register.html', {'form': form})


	
def register2(request):
	if request.method == 'POST':
		form = OwnerForm(request.POST)
		form2 = RestaurantForm(request.POST,request.FILES)
		if form.is_valid():
			if form2.is_valid():
				data=form.cleaned_data
				data['editor']=1
				token=randomword(20)
				data['token']=token
				data['first_name']="Restaurant"
				data['last_name']="Owner"
				# validatePassword(data['password'])
				u=saveUser(data)
				a=form2.save(commit=False)
				a.user=u
				a.save()
				sendConfirm(u)
				return redirect('confirm')
	else:
		form = OwnerForm()
		form2 = RestaurantForm()
	return render(request, 'account/register2.html', {'form': form,'form2':form2})

def verify(request,token):
	user=get_object_or_404(User,token=token)
	user.token=''
	user.save()
	return redirect('login')

def sendConfirm(user):
	if user.token:
		body='''<h2>Ciibbo</h2>
		<h4>Hello '''+user.first_name+''',</h4>
		<p>Welcome to Ciboo, please follow the <a href="'''+SITE_URL+'''/verify/'''+user.token+'''">Link</a> to verify your account.</p>'''
		email = EmailMessage('Verify Registration for Ciibbo', body, to=[user.email])
		email.content_subtype = "html"
		email.send()

def sendForgot(user):
	forgot=randomword(20)
	user.forgot=forgot
	user.save()
	body='''<h2>Ciibbo</h2>
	<h4>Hello '''+user.first_name+''',</h4>
	<p>Please follow the <a href="'''+SITE_URL+'''/reset/'''+forgot+'''">Link</a> to reset your account password.</p>'''
	print body
	email = EmailMessage('Reset Password for Ciibbo', body, to=[user.email])
	email.content_subtype = "html"
	email.send()

def testEmail(request):
	body='test'
	email = EmailMessage('Test Email for Ciibbo', body, to=['bharatbgarg4@gmail.com'])
	email.content_subtype = "html"
	email.send()
	return redirect('/')

def confirm(request):
	if request.method == 'POST':
		form = ConfirmForm(request.POST)
		if form.is_valid():
			data=form.cleaned_data
			email=data['email']
			print email
			user = get_object_or_404(User, email=email)
			if user.token:
				sendConfirm(user)
	else:
		form = ConfirmForm()
	return render(request, 'account/confirm.html', {'form': form})

def forgot(request):
	no=0
	nomail=0
	if request.method == 'POST':
		no=1
		form = ConfirmForm(request.POST)
		if form.is_valid():
			data=form.cleaned_data
			email=data['email']
			users = User.objects.filter(email=email)
			if len(users):
				no=0
				user=users[0]
				try:
					sendForgot(user)
					return redirect('login')
				except:
					nomail=1
	else:
		form = ConfirmForm()
	return render(request, 'account/forgot.html', {'form': form,'no':no,'nomail':nomail})

def reset(request,token):
	user=0
	if token:
		users = User.objects.filter(forgot=token)
		if len(users):
			user=users[0]
			if request.method == 'POST':
				form = ResetForm(request.POST)
				# data=form.cleaned_data
				print 'data'
				p1=request.POST['password']
				p2=request.POST['confirm_password']
				if p1==p2:
					user.forgot=0
					user.set_password(p1)
					user.save()
					return redirect('login')
	form = ResetForm()
	return render(request, 'account/reset.html', {'user': user,'form':form})

@onlyAdmin
def deleteUser(request,id):
	user = get_object_or_404(User, id=id)
	user.delete()
	return redirect(request.META.get('HTTP_REFERER'))
	
	
@onlyAdmin	
def approveUser(request,id):
	Restaurant.objects.filter(user_id=id).update(approve=True)
	rest1=Restaurant.objects.filter(user_id=id)
	dd1=rest1.values('title')
	x1=str(dd1)
	y1=x1.replace("<QuerySet [{'title': u'"," ")
	z1=y1.replace("'}]>","")
	rest=User.objects.filter(id=id)
	dd=rest.values('email')
	x=str(dd)
	y=x.replace("<QuerySet [{'email': u'"," ")
	z=y.replace("'}]>","")
	#data['object_list'] = rest
	print(z1)
	body='''<h2>Ciibbo</h2>
	<h4>Hello '''+z1+''',</h4>
	<p>Congrats your Business is approved .</p>'''
	#print body
	email = EmailMessage('Business Approval', body, to=[z])
	email.content_subtype = "html"
	email.send()
	
	
	return redirect(request.META.get('HTTP_REFERER'))



@onlyAdmin	
def declineUser(request,id):
	Restaurant.objects.filter(user_id=id).update(approve=False)
	rest1=Restaurant.objects.filter(user_id=id)
	dd1=rest1.values('title')
	x1=str(dd1)
	y1=x1.replace("<QuerySet [{'title': u'"," ")
	z1=y1.replace("'}]>","")
	rest=User.objects.filter(id=id)
	dd=rest.values('email')
	x=str(dd)
	y=x.replace("<QuerySet [{'email': u'"," ")
	z=y.replace("'}]>","")
	#data['object_list'] = rest
	print(z1)
	body='''<h2>Ciibbo</h2>
	<h4>Hello '''+z1+''',</h4>
	<p>Sorry your Business is Declined.</p>'''
	#print body
	email = EmailMessage('Business Decline', body, to=[z])
	email.content_subtype = "html"
	email.send()
	
	
	return redirect(request.META.get('HTTP_REFERER'))
	
def update(request, template_name='account/update_profile.html'):
    try:
        user_profile = User.objects.get(id=request.user.id)
    except User.DoesNotExist:
        return HttpResponse("invalid user_profile!")
    if request.method == 'POST':
        #form = EditProfile(request.POST,instance=request.user)
        update_profile_form = EditProfile(request.POST or None, request.FILES or None, instance=user_profile)

        if 	update_profile_form.is_valid():
			#update_profile_form.avatar=request.FILES['avatar']
			#print(request.FILES['avatar'])
			update_profile_form.save()
			
          #  messages.success(request,('Your profile was successfully updated!'))
			return redirect('/dashboard') 
        else:
                messages.error(request,('Please correct the error below.'))       

       
    else:
        # form = EditProfile(instance=request.user) 
         update_profile_form = EditProfile(instance=user_profile)  
    return render(request, template_name, {'update_profile_form': update_profile_form})	
# def profile(request,id):
# 	user = get_object_or_404(User, id=id)	
# 	form = OwnerForm(request.POST or None)
# 	return render(request, 'account/profile.html',{'user':user})