from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import Group, PermissionsMixin
from rest_framework import serializers
from django import forms
from django.core.mail import send_mail
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.base_user import BaseUserManager
from dj import settings

class UserManager(BaseUserManager):
	use_in_migrations = True
	def _create_user(self,username, email, password, **extra_fields):
		if not email:
			raise ValueError('The given email must be set')
		email = self.normalize_email(email)
		user = self.model(email=email, **extra_fields)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_user(self,username, email, password=None, **extra_fields):
		extra_fields.setdefault('is_superuser', False)
		return self._create_user(email,username, password, **extra_fields)

	def create_superuser(self, email, password, **extra_fields):
		extra_fields.setdefault('is_superuser', True)
		if extra_fields.get('is_superuser') is not True:
			raise ValueError('Superuser must have is_superuser=True.')
		return self._create_user(email, password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
	email = models.EmailField(unique=True)
	first_name = models.CharField(max_length=40, blank=True,null=True)
	last_name = models.CharField(max_length=40, blank=True,null=True)
	token = models.CharField(max_length=30, blank=True,null=True)
	forgot = models.CharField(max_length=30, blank=True,null=True)
	admin = models.BooleanField(default=False)
	editor = models.BooleanField(default=False)
	active = models.BooleanField(_('active'), default=True)
	date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
	avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)
	addres = models.CharField(verbose_name='Address',max_length=240, blank=True,null=True)
	city = models.CharField(verbose_name='City',max_length=240, blank=True,null=True)
	state = models.CharField(verbose_name='State',max_length=240, blank=True,null=True)
	zip = models.CharField(verbose_name='Zip',max_length=240, blank=True,null=True)
	mobile = models.IntegerField(verbose_name='Phone no',blank=True,null=True)
	bio = models.TextField(max_length=500, blank=True)
	birth_date = models.DateField(null=True, blank=True)
	def __unicode__(self):
		return self.email
	objects = UserManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	class Meta:
		verbose_name = _('user')
		verbose_name_plural = _('users')

	def email_user(self, subject, message, from_email=None, **kwargs):
		send_mail(subject, message, from_email, [self.email], **kwargs)

class Address(models.Model):
	title = models.CharField(max_length=20)
	house = models.CharField(verbose_name='Address',max_length=200)
	suite = models.CharField(verbose_name='Apt/Suite',max_length=80,null=True, blank=True)
	city = models.CharField(max_length=80)
	state = models.CharField(max_length=40)
	zip_code = models.CharField(max_length=20)
	phone = models.CharField(max_length=20)
	cross_street = models.CharField(max_length=80,null=True, blank=True)
	instructions = models.CharField(max_length=400,null=True, blank=True)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

class UserSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = User
		fields = ('url', 'username', 'email', 'groups')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Group
		fields = ('url', 'name')


class AddressForm(forms.ModelForm):
	class Meta:
		model = Address
		fields = ['title','house','suite','city','state','zip_code','phone','cross_street','instructions']