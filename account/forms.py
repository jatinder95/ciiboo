from django import forms
from .models import User
from django.utils.translation import ugettext_lazy as _

class LoginForm(forms.Form):
	username = forms.EmailField(label='Email', max_length=100)
	password = forms.CharField(label='Password', max_length=128,widget=forms.PasswordInput()) 
	
	#def clean(self):
	#	username = self.cleaned_data.get('username')
	#	password = self.cleaned_data.get('password')
	#	user = authenticate(username=username, password=password)
	#	if not user or not user.is_active:
	#		raise forms.ValidationError("Sorry, that login was invalid. Please try again.")
	#	return self.cleaned_data

	def __init__(self, *args, **kwargs):
		super(LoginForm, self).__init__(*args, **kwargs)
		for f in self.fields:
			self.fields[f].widget.attrs['placeholder'] = self.fields[f].label

class RegisterForm(forms.ModelForm):
	password = forms.CharField(label='Password', max_length=128,widget=forms.PasswordInput())
	confirm_password = forms.CharField(label='Confirm Password', max_length=128,widget=forms.PasswordInput())
	def __init__(self, *args, **kwargs):
		super(RegisterForm, self).__init__(*args, **kwargs)
		for f in self.fields:
			self.fields[f].widget.attrs['placeholder'] = self.fields[f].label
	class Meta:
		model = User
		widgets = {
            'first_name': forms.TextInput(attrs={'required':'required','class': 'form-control','placeholder':'Enter First Name'})
			,'addres': forms.TextInput(attrs={'required':'required'})
			,'city': forms.TextInput(attrs={'required':'required'})
			,'state': forms.TextInput(attrs={'required':'required'})
			,'mobile': forms.TextInput(attrs={'required':'required'})
			,'zip': forms.TextInput(attrs={'required':'required'})
            ,'last_name': forms.TextInput(attrs={'required':'required','class': 'form-control','placeholder':'Enter Last Name'})}
        
		fields = ['first_name','last_name','email', 'addres','city','state','zip','mobile','password']
	def clean(self):
			cleaned_data = super(RegisterForm, self).clean()
			password = cleaned_data.get("password")
			confirm_password = cleaned_data.get("confirm_password")
			validatePassword(password)
			if password != confirm_password:
				raise forms.ValidationError(
					"Password and Confirm Password does not match"
				)
				
#def clean(self):
#			if 'password' in self.cleaned_data and 'confirm_password' in self.cleaned_data:
#				if self.cleaned_data['password'] != self.cleaned_data['confirm_password']:
#					raise forms.ValidationError(_("The two password fields did not match."))
#			return self.cleaned_data			

class EditProfile(forms.ModelForm):

   

    class Meta:
        model =User
        fields = {
        'first_name','last_name','addres','avatar','addres','city','state','zip','mobile'
        
        
        
        }

class OwnerForm(forms.ModelForm):
	password = forms.CharField(label='Password', max_length=128,widget=forms.PasswordInput())
	confirm_password = forms.CharField(label='Confirm Password', max_length=128,widget=forms.PasswordInput())
	def __init__(self, *args, **kwargs):
		super(OwnerForm, self).__init__(*args, **kwargs)
		for f in self.fields:
			self.fields[f].widget.attrs['placeholder'] = self.fields[f].label
	class Meta:
		model = User
		fields = ['email', 'password']
	def clean(self):
			cleaned_data = super(OwnerForm, self).clean()
			password = cleaned_data.get("password")
			confirm_password = cleaned_data.get("confirm_password")
			validatePassword(password)
			if password != confirm_password:
				raise forms.ValidationError(
					"Password and Confirm Password does not match"
				)

class ConfirmForm(forms.Form):
	def __init__(self, *args, **kwargs):
			super(ConfirmForm, self).__init__(*args, **kwargs)
			for f in self.fields:
				self.fields[f].widget.attrs['placeholder'] = self.fields[f].label
	email = forms.EmailField(label='Email', max_length=100)

class ResetForm(forms.Form):
	def __init__(self, *args, **kwargs):
			super(ResetForm, self).__init__(*args, **kwargs)
			for f in self.fields:
				self.fields[f].widget.attrs['placeholder'] = self.fields[f].label
	password = forms.CharField(widget=forms.PasswordInput)
	confirm_password = forms.CharField(widget=forms.PasswordInput)


def validatePassword(password):
	special_characters = "[~\!@#\$%\^&\*\(\)_\+{}\":;'\[\]]"
	if len(password)<8:
		raise forms.ValidationError("Password must be longer than 8 characters.")
	if not any(char.isdigit() for char in password):
		raise forms.ValidationError("Password must contain at least 1 digit.")
	if not any(char.isalpha() for char in password):
		raise forms.ValidationError("Password must contain at least 1 letter.")
	if not any(char.istitle() for char in password):
		raise forms.ValidationError("Password must contain at least 1 Capital Letter.")
	if not any(char in special_characters for char in password):
		raise forms.ValidationError("Password must contain at least 1 special character.")
	return 1