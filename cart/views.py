# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from account.models import User
from django.shortcuts import render
from django.shortcuts import render,redirect,get_object_or_404
from restaurant.models import Order,Item,Cuisine
from account.models import AddressForm,Address
from dj.settings import STRIPE_KEY, STRIPE_SECRET
#import stripe, json,requests

#stripe_keys = {
 # 'secret_key': STRIPE_SECRET,
 # 'publishable_key': STRIPE_KEY
 ##}

#stripe.api_key = stripe_keys['secret_key']

def getOrder(id=0,status=0):
	order = get_object_or_404(Order, id=id)
	if status:
		order.status=status
		order.save()
	items= Item.objects.filter(order=order)
	total=0
	for i in items:
		amount=i.cuisine.price*i.quantity
		i.amount=amount
		total=total+amount
	return {'order':order,'items':items,'total':total}

def order(request,id):
	users=User.objects.filter(id=request.user.id)
	addresses= Address.objects.filter(user=request.user)
	form = AddressForm()
	data=getOrder(id,1)
	data['form']=form
	data['addresses']=addresses
	data['users']=users
	return render(request,'order/index.html',data)

def dele(request,id):
	item = get_object_or_404(Item,id=id)
	item.delete()
	return redirect(request.META.get('HTTP_REFERER'))
	
def delete1(request,id,pid):
	#addresses= Address.objects.filter(user=request.user)
	item = get_object_or_404(Item, id=pid)
	item.delete()
	return redirect(request.META.get('HTTP_REFERER'))
	
def delete(request,id,pid):
	address = get_object_or_404(Address, id=pid)
	address.delete()
	return redirect(request.META.get('HTTP_REFERER'))
	
def selectAddress(request,id,aid):
	order = get_object_or_404(Order, id=id)
	address = get_object_or_404(Address, id=aid)
	order.address=address
	order.save()
	return redirect(request.META.get('HTTP_REFERER'))

def addAddress(request,id):
	order = get_object_or_404(Order, id=id)
	form = AddressForm(request.POST)
	if form.is_valid():
		a=form.save(commit=False)
		a.user=request.user
		a.save()
	return redirect(request.META.get('HTTP_REFERER'))

def addAddress1(request,id):
	order = get_object_or_404(Order, id=id)
	q1 = request.POST.getlist('a1')
	q2 = request.POST.getlist('a2')
	q3 = request.POST.getlist('a3')
	q4 = request.POST.getlist('a4')
	q5 = request.POST.getlist('a5')
	q6 = request.POST.getlist('a6')
	q7 = request.POST.getlist('a7')
	q8 = request.POST.getlist('a8')
	q9 = request.POST.getlist('a9')
	
	
	c = Address(user=request.user, title=q1, house=q2,suite=q3,city=q4, state=q5,zip_code=q6,phone=q7,cross_street=q8,instructions=q9)
	c.save()
	return redirect(request.META.get('HTTP_REFERER'))
	
def addAddupdate(request,id,pid):
		
	order = get_object_or_404(Address, id=pid)
	form = AddressForm(request.POST,instance=order)
	if form.is_valid():
		a=form.save(commit=False)
		a.user=request.user
		a.save()
	return redirect(request.META.get('HTTP_REFERER'))
	

def checkout(request,id):
	form = AddressForm()
	data=getOrder(id,2)
	data['form']=form
	
	data['stripekey']=STRIPE_KEY
	return render(request,'order/checkout.html',data)

def payment(request,id):
	order=getOrder(id)
	amount = int(order['total']*100)
	customer = stripe.Customer.create(
		email=request.user.email,
		source=request.POST['stripeToken']
	)
	charge = stripe.Charge.create(
		customer=customer.id,
		amount=amount,
		currency='usd',
		description='Ciiboo Charge'
	)
	# Simple payouts- depricated
	# recipient = stripe.Recipient.create(
	#   name="John Doe",
	#   type="individual",
	#   email="payee@example.com",
	#   card=token_id
	# )
	# payout = stripe.Payout.create(
	#     amount=1000, # Amount in cents
	#     currency="usd",
	#     recipient=recipient.id,
	#     card=card_id,
	#     statement_descriptor="JUNE SALES"
	# )
	# Create Account
	# acct = stripe.Account.create(
	#   country="US",
	#   type="custom"
	# )
	# print acct
	#Connect payout
	charge = stripe.Charge.create(
		amount=amount,
		currency="usd",
		source="tok_visa",
		destination={
		"account": order.restaurant.stripeId,
		}
	)
	print charge
	data=getOrder(id,4)
	return redirect('/order/'+id)

def status(request,id):
	order = get_object_or_404(Order, id=id)
	return render(request,'order/done.html',{'order':order})


def stripeconnect(request):
	print request
	auth_code=request.GET['AUTHORIZATION_CODE']
	req=requests.post('https://connect.stripe.com/oauth/token',{'grant_type':'authorization_code','code':auth_code,'client_secret':STRIPE_SECRET})
	data=json.loads(req.content)
	print data
	if 'stripe_user_id' in data:
		account_id=data['stripe_user_id']
		rest=request.user.restaurant
		rest.stripeId=account_id
		rest.save()
	return redirect('/dashboard/payments/')
