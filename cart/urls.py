from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^(?P<id>[0-9]+)$', views.status, name='done'),
	url(r'^(?P<id>[0-9]+)/checkout$', views.checkout, name='checkout'),
	url(r'^(?P<id>[0-9]+)/delivery$', views.order, name='delivery'),
	url(r'^(?P<id>[0-9]+)/dele$', views.dele, name='dele'),
	url(r'^(?P<id>[0-9]+)/pickup$', views.checkout, name='pickup'),
	url(r'^(?P<id>[0-9]+)/address/(?P<pid>[0-9]+)/delete1', views.delete1, name='delete1'),
	url(r'^(?P<id>[0-9]+)/address/(?P<pid>[0-9]+)/delete', views.delete, name='delete'),
	#url(r'^(?P<id>[0-9]+)/update$', views.addAddupdate, name='addAddupdate'),
	url(r'^(?P<id>[0-9]+)/checkout/(?P<pid>[0-9]+)/update', views.addAddupdate, name='addAddupdate'),
	#(r'^(?P<id>[0-9]+)/cuisine/(?P<pid>[0-9]+)/delete', views.deleteCuisine, name='deleteCuisine'),
	url(r'^(?P<id>[0-9]+)/addAddress$', views.addAddress, name='addAddress'),
	url(r'^(?P<id>[0-9]+)/selectAddress/(?P<aid>[0-9]+)$', views.selectAddress, name='selectAddress'),
    url(r'^(?P<id>[0-9]+)/payment$', views.payment, name='payment'),
	url(r'^(?P<id>[0-9]+)/stripeconnect$', views.stripeconnect, name='stripeconnect'),
]