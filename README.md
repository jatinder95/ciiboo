# Ciibo

## Install Instructions

#### First Install python, pip, nodejs and database
```
sudo apt-get install python-pip
sudo npm install -g gulp
git clone git@bitbucket.org:bharatbgarg4/ciiboo.git
cd ciiboo
sudo pip install -r requirements.txt
npm install
cp dj/private.example.py dj/private.py
---- Optionally Change Database and other settings in private.py ----
python manage.py makemigrations
python manage.py migrate
python manage.py runserver 8040
```
#### Site should be live at localhost:8040
###### Additionally run *gulp* in seperate terminal to run css, js compiler