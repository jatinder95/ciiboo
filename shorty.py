from django.http import Http404  
from restaurant.models import Restaurant
from django.shortcuts import render,redirect,get_object_or_404

def readFile(url):
	return open(url,'r').read()

def writeFile(url,content):
	file=open(url, 'w')
	file.write(content)
	file.close()

def onlyAdmin(fun):
	def newfun(request, *args, **kwargs):
		if request.user.is_authenticated():
			if request.user.admin:
				return fun(request, *args, **kwargs)
		raise Http404()
	return newfun


def onlyEditor(fun):
	def newfun(request, *args, **kwargs):
		if request.user.is_authenticated():
			if request.user.editor:
				return fun(request, *args, **kwargs)
		raise Http404()
	return newfun

def onlyOwner(fun):
	def newfun(request, *args, **kwargs):
		id=kwargs['id']
		if request.user.is_authenticated():
			if request.user.editor:
				restaurant = get_object_or_404(Restaurant, id=id)
				if restaurant.user==request.user:
					return fun(request, *args, **kwargs)
		raise Http404()
	return newfun

def onlyAuth(fun):
	def newfun(request, *args, **kwargs):
		if request.user.is_authenticated():
			return fun(request, *args, **kwargs)
		raise Http404()
	return newfun