from django.shortcuts import get_object_or_404, render, redirect
from .models import Article,ArticleForm

def yearList(request, year):
	a_list = Article.objects.filter(pub_date__year=year)
	context = {'year': year, 'articles': a_list}
	return render(request, 'blog/index.html', context)

def show(request, article_id):
	article = get_object_or_404(Article, pk=article_id)
	return render(request, 'blog/show.html', {'article': article})

def index(request):
	articles = Article.objects.all()
	print 'articles'
	print articles
	context = {'articles': articles}
	return render(request, 'blog/index.html', context)

def create(request):
	if request.method == 'POST':
		form = ArticleForm(request.POST)
		if form.is_valid():
			a=form.save(commit=False)
			a.user=request.user
			a.save()
			return redirect('/articles/')
	else:
		form = ArticleForm()
	return render(request, 'blog/create.html', {'form': form})

def edit(request,id):
	article = get_object_or_404(Article, id=id)
	form = ArticleForm(request.POST or None, instance=article)
	if form.is_valid():
		form.save()
		return redirect('/articles')
	return render(request, 'blog/edit.html', {'form': form,'article':article})

def delete(request,id):
	article = get_object_or_404(Article, id=id)
	article.delete()
	return redirect('/articles/')