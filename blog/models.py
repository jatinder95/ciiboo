from __future__ import unicode_literals

from django.db import models
from django import forms
from dj import settings

class Article(models.Model):
	created = models.DateField(auto_now_add=True, blank=True)
	title = models.CharField(max_length=200)
	content = models.TextField()
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)



class ArticleForm(forms.ModelForm):
	class Meta:
		model = Article
		fields = ['title','content']