# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect

from django.http import HttpResponse
from restaurant.models import Cuisine, Restaurant, Section
from restaurant.forms import RestaurantForm,CuisineForm,SectionForm
from django.db.models import Q
import shorty,random
from faker import Factory
from account.models import User
from account.views import saveUser,randomword
try:
	from dj.settings import DUMMY
except:
	DUMMY=0
#fff
def home(request):
	if request.user.is_authenticated:
		if request.user.editor:
			return redirect('/dashboard');
	return render(request, 'pages/home.html',{'homepage':1})

def about(request):
	return render(request, 'pages/about.html')

def search(request,location,food):
	if request.POST:
		print request.POST
		location=request.POST['location']
		food=request.POST['food']
		return redirect('/search/'+location+'/'+food)	
	restaurants=Restaurant.objects.filter(Q(country__icontains=location) | Q(street__icontains=location) | Q(city__icontains=location) | Q(state__icontains=location),Q(approve=True))
	sres=[]
	for i in restaurants:
		cc=0
		sections=Section.objects.filter(restaurant=i)
		for j in sections:
			ccc=Cuisine.objects.filter(section=j).filter(Q(title__icontains=food) | Q(content__icontains=food)).count()
			cc=cc+ccc
		# print cc
		if cc:
			sres.append(i)
	context={'location':location,'food':food,'restaurants':sres}
	return render(request, 'restaurant/index.html', context)
	return render(request, 'pages/search.html',context)


def saveRestaurant(data):
	print 'data'
	r=Restaurant(**data)
	r.save()
	return r

def seed(request):
	fake = Factory.create()
	u=User.objects.all()
	for i in u:
		i.delete()
	r=Restaurant.objects.all()
	for i in r:
		i.delete()
	c=Cuisine.objects.all()
	for i in c:
		i.delete()
	u2=saveUser({'email':'jatinder@gmail.com','password':'Jatin@95','admin':1,'editor':1,'first_name':'ciibbo admin'})
	nr=RestaurantForm({
		'title':'Test Restaurant',
		'country':0,
		'street':'test',
		'city':'test',
		'state':'test',
		'zip_code':'00000',
		'phone':fake.phone_number(),
		'category':fake.sentence(),
		'content':fake.sentence(),
		'rating':float("{0:.2f}".format(random.uniform(2,5))),
		'cost':random.randrange(2,6),
		})
	a=nr.save(commit=False)
	a.user=u2
	a.save()
	if DUMMY:
		u3=saveUser({'email':'admin@gmail.com','password':'Jatin@95','admin':1,'editor':1,'first_name':'admin'})
		u=saveUser({'email':'user@gmail.com','password':'qwert','admin':0,'editor':0,'first_name':'test'})
		u=saveUser({'email':'ssingh2222@gmail.com','password':'qwert','admin':0,'editor':0,'first_name':'test'})
		u4=saveUser({'email':'bharatbgarg4@gmail.com','password':'qwert','admin':0,'editor':0,'first_name':'test'})
		u1=saveUser({'email':'business@gmail.com','password':'qwert','admin':0,'editor':1,'first_name':'business'})
		for i in range(30):
			name=fake.name().split(' ')
			b={}
			b['email']=fake.email()
			b['password']='qwert'
			b['admin']=1
			b['editor']=random.choice([1,0,0])
			b['first_name']=name[0]
			b['last_name']=name[1]
			saveUser(b)
		r=shorty.readFile('restaurantList').split('\n')
		c=shorty.readFile('cuisinesList').split('\n')
		s=['Popular','Starters','Veg','Non Veg','Drinks','Desserts']
		for i in r:
			u=random.choice([u1,u2,u3,u4])
			address=fake.address().split('\n')
			nr=RestaurantForm({
				'title':i,
				'country':0,
				'street':address[0],
				'city':address[1],
				'state':address[1],
				'zip_code':'00000',
				'phone':fake.phone_number(),
				'category':fake.sentence(),
				'content':fake.sentence(),
				'rating':float("{0:.2f}".format(random.uniform(2,5))),
				'cost':random.randrange(2,6),
				})
			a=nr.save(commit=False)
			a.user=u
			a.save()
			for o in s:
				nc=SectionForm({
					'title':o,
					})
				b=nc.save(commit=False)
				b.restaurant=a
				b.save()
				for j in range(5):
					nc=CuisineForm({
						'title':random.choice(c),
						'content':fake.sentence(),
						'price':float("{0:.2f}".format(random.uniform(2,55))),
						})
					cc=nc.save(commit=False)
					cc.section=b
					cc.save()
	return redirect('/');