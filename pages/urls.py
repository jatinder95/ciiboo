from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^about/$', views.about),
    url(r'^seed/$', views.seed),
    url(r'^search/$', views.search,{'location':0,'food':0}),
    url(r'^search/(?P<location>.+)/(?P<food>.+)/$', views.search),
]