from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.profile),
	url(r'^users/$', views.users),
	# url(r'^orders/$', views.orders),
	url(r'^info/$', views.info),
	url(r'^menu/$', views.menu),
	url(r'^orders/active/$', views.active_orders),
	url(r'^orders/old/$', views.old_orders),
	url(r'^approve/', views.approve),
	url(r'^payments/$', views.payments),
	url(r'^connectstripe/$', views.connectstripe),
	url(r'^summary/$', views.summary),
	url(r'^orders/(?P<id>[0-9]+)/status/(?P<status>[0-9]+)$', views.statusUpdate),
]