# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect,get_object_or_404
from account.models import User
from restaurant.models import Order,Item,Restaurant,Section,Cuisine,ost,countries
from restaurant.forms import SectionForm
from shorty import onlyOwner,onlyEditor,onlyAdmin,onlyAuth
from dj.settings import CONNECT_ID
@onlyAdmin
def users(request):
	users=User.objects.all()
	
	print(users)
	return render(request, 'dashboard/users.html', {'users': users})

def getOrder(order):
	order.items= Item.objects.filter(order=order)
	total=0
	for i in order.items:
		amount=i.cuisine.price*i.quantity
		i.amount=amount
		total=total+amount
	order.total=total
	try:
		order.status=ost[order.status]
	except:
		print order.status
		order.status=ost[8]
	return order

def active_orders(request):
	orders=0
	restaurant=0
	if request.user.editor:
		restaurant=Restaurant.objects.filter(user=request.user)
		if restaurant:
			restaurant=restaurant[0]
			orders=Order.objects.filter(restaurant=restaurant)
	else:
		orders=Order.objects.filter(user=request.user)
	orders=orders.exclude(status=8)
	for o in orders:
		o=getOrder(o)
	return render(request, 'dashboard/orders.html', {'orders': orders,'restaurant':restaurant})
	
@onlyAdmin
def approve(request):
	users=User.objects.filter(editor=True)
	
	return render(request, 'dashboard/approve.html', {'users': users})
	
def old_orders(request):
	orders=0
	restaurant=0
	if request.user.editor:
		restaurant=Restaurant.objects.filter(user=request.user)
		if restaurant:
			restaurant=restaurant[0]
			orders=Order.objects.filter(restaurant=restaurant)
	else:
		orders=Order.objects.filter(user=request.user)
	orders=orders.filter(status=8)
	for o in orders:
		o=getOrder(o)
	return render(request, 'dashboard/orders.html', {'orders': orders,'restaurant':restaurant})

def profile(request):
	restaurant=0
	if request.user.editor:
		restaurant=Restaurant.objects.filter(user=request.user)
		if restaurant:
			restaurant=restaurant[0]
			restaurant.country=dict(countries)[restaurant.country]
		return render(request, 'dashboard/business.html', {'restaurant':restaurant})
	return render(request, 'dashboard/profile.html', {'restaurant':restaurant})

def info(request):
	restaurant=0
	if request.user.editor:
		restaurant=Restaurant.objects.filter(user=request.user)
		if restaurant:
			restaurant=restaurant[0]
			restaurant.country=dict(countries)[restaurant.country]
	return render(request, 'dashboard/info.html', {'restaurant':restaurant})

def menu(request):
	sectionform = SectionForm()
	restaurant=Restaurant.objects.filter(user=request.user)
	if restaurant:
		restaurant=restaurant[0]
	restaurant.country=dict(countries)[restaurant.country]
	sections= Section.objects.filter(restaurant=restaurant)
	for section in sections:
		section.cuisines=Cuisine.objects.filter(section=section)
	return render(request, 'dashboard/menu.html', {'restaurant': restaurant,'sections':sections,'sectionform':sectionform})

def statusUpdate(request,id,status):
	order=get_object_or_404(Order, id=id)
	order.status=status
	order.save()
	return redirect('/dashboard/orders/active')

def summary(request):
	orders=0
	restaurant=0
	orders=Order.objects.filter(user=request.user)
	orders=orders.filter(status=8)
	for o in orders:
		o=getOrder(o)
	return render(request, 'dashboard/summary.html', {'orders': orders,'restaurant':restaurant})

def payments(request):
	orders=0
	restaurant=Restaurant.objects.filter(user=request.user)
	if restaurant:
		restaurant=restaurant[0]
	orders=Order.objects.filter(restaurant=restaurant)
	orders=orders.filter(status=8)
	for o in orders:
		o=getOrder(o)
	return render(request, 'dashboard/payments.html', {'orders': orders,'restaurant':restaurant,'connectId':CONNECT_ID})


def connectstripe(request):
	print request