var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglifyjs');
var runSequence = require('run-sequence');
var rev = require('gulp-rev');

var input = './resources/sass/app.scss';
var wa = './resources/**/*';
var output = './static/assets';
var jsRaw=[
'./node_modules/jquery/dist/jquery.min.js',
'./node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
'./resources/js/main.js',
]

gulp.task('default', ['sass','script','watch']);
// gulp.task('default', function() {
// 	runSequence(
// 		['sass', 'script'],
// 		'watch'
// 		);
// });

// gulp.task('rev', function(){

// 	// so we need to set it explicitly:
// 	gulp.src([output+'/app.css', output+'/app.js'], {base: 'assets'})
// 		.pipe(gulp.dest(output))  // copy original assets to build dir
// 		.pipe(rev())
// 		.pipe(gulp.dest(output))  // write rev'd assets to build dir
// 		.pipe(rev.manifest())
// 		.pipe(gulp.dest(output));  // write manifest to build dir
// 	});


gulp.task('watch', function() {
	return gulp
	.watch([wa], ['sass','script'])
	// .watch([wa], function() {
	// 	runSequence(
	// 		['sass', 'script']
	// 		);
	// })
	.on('change', function(event) {
		// console.log('watcher running');
		console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
	});
});

gulp.task('sass', function () {
	return gulp
	.src(input)
	.pipe(sass())
	.pipe(autoprefixer())
	.pipe(gulp.dest(output));
});

gulp.task('script', function() {
	return gulp.src(jsRaw)
	.pipe(concat('app.js'))
	.pipe(gulp.dest(output));
});

gulp.task('prod',['cssProd','jsProd']);
// gulp.task('prod',function() {
// 	runSequence(
// 		['cssProd','jsProd']
// 		);
// });

gulp.task('cssProd', function(){
	return gulp
	.src(input)
	.pipe(sass({ outputStyle: 'compressed' }))
	.pipe(autoprefixer())
	.pipe(gulp.dest(output));
});

gulp.task('jsProd', function() {
	return gulp.src(jsRaw)
	.pipe(uglify('app.js'))
	.pipe(gulp.dest(output));
});